var Encore = require('@symfony/webpack-encore');

Encore
    // the project directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // the public path used by the web server to access the previous directory
    .setPublicPath('/build')
    // the public path you will use in Symfony's asset() function - e.g. asset('build/some_file.js')
    .setManifestKeyPrefix('build/')

    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())

    // the following line enables hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())

    // define the assets of the project
    .addEntry('base', './assets/js/base.js')
    .addEntry('index', './assets/js/index.ts')
    .addEntry('cruce_horarios', './assets/js/cruce_horarios.tsx')
    .addEntry('course_detail', './assets/js/course_detail.tsx')
    .addEntry('teacher_detail', './assets/js/teacher_detail.tsx')
    .addEntry('course_list', './assets/js/course_list.js')
    .addStyleEntry('schedule', './assets/css/schedule.scss')
    .addStyleEntry('teacher_details', './assets/css/teacher_detail.scss')

    .enableTypeScriptLoader()
    .enableReactPreset()
    .enableSassLoader()
    .autoProvideVariables({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery',
        Popper: 'popper.js',
    })
;

module.exports = Encore.getWebpackConfig();
