'use strict'
const React = require('react');
const ReactDOM =  require('react-dom');

import ScheduleTable from "./components/ScheduleTable";

declare const courseId: string;
declare const pathCourseListJson: string;
declare const pathSectionListJson: string;
declare const pathLocationListJson: string;

interface Course {
  id: string;
  name: string;
  cycle: number;
  numCredits: number;
  evaluationSystem: string;
  departmentId: string;
  sectionsId: number[];
}

interface Section {
  course: string;
  section: string;
  locations: Location[];
}

interface Location {
  id: number;
  sectionId: number;
  type: string;
  classroomsId: string;
  weekday; number;
  beginHour: number;
  endHour: number;
}

class App extends (React.Component as { new (any): any }) {
  constructor(props) {
    super(props)
    this.state = {
      error: null,
      isLoaded: false,
      sectionList: [],
      course: {},
    }
  }

  componentDidMount() {
    fetch(pathSectionListJson + '?course_id=' + this.props.courseId)
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            sectionList: result.sectionList,
            course: result.course,
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error,
          });
        }
      )
  }

  render() {
    const {
      error,
      isLoaded,
      sectionList,
      course,
    } = this.state;

    if (error)
      return <div>Error al descargar secciones: {error.message}</div>;
    else if (!isLoaded)
      return <div>Descargando secciones...</div>;

    return sectionList.map(section => (
      <ScheduleTable
        key={section.section}
        locationList={section.locations}
        title={course.id + section.section} />
    ));
  }
}

ReactDOM.render(
  <App courseId={courseId} />,
  document.getElementById('root')
);
