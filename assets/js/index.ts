const $ = require('jquery');
import 'bootstrap';

require('../css/index.scss');

declare const images: string[];

// Getting navbar of class .navbar
const navbar:HTMLElement = document.getElementsByTagName('nav')[0] as HTMLElement;

// Add event to change navbar color if scrolled
document.addEventListener('scroll', (e) => {
  const scroll_start = 64;

  // if (e.pageY > scroll_start) and it's not colored or
  // if (e.pageY < scroll_start) and it's colored
  if((window.scrollY > scroll_start) == (navbar.style.backgroundColor == '')) {
    // Change color
    navbar.style.backgroundColor = navbar.style.backgroundColor ? '': '#711610';
  }
});


const carousel_img:HTMLCollectionOf<HTMLDivElement> = document.getElementsByClassName('carousel-img') as HTMLCollectionOf<HTMLDivElement>;

for (let i = 0; i < images.length; i++) {
    carousel_img[i].style['background-image'] = 'url(' + images[i] + ')';
}

$('.carousel').carousel({
  pause: false,
  interval: 6000
});

$(window).on('load', () => {
    $('#modal').modal('show');
    console.log("window onload");
});
