const $ = require('jquery');
require('bootstrap');
const React = require('react');

declare const pathTeacherDetail: string;

export default class ScheduleTable extends (React.Component as { new(any): any }) {
  constructor(props) {
    super(props)
  }

  getSchedule(): Location[][][] {
    const {locationList} = this.props;

    // Initialize schedule TODO: Change any
    const schedule = new Array<Array<Array<any>>>();
    for (let i = 8; i < 22; i++) {
      schedule[i] = new Array<Array<any>>();
      for (let j = 1; j <= 6; j++)
        schedule[i][j] = new Array<any>()
    }

    // Fill schedule
    for (let loc of locationList) {
      const wd = loc.weekday;
      for (let h = loc.beginHour; h < loc.endHour; h++)
        schedule[h][wd].push(
          <a
            key={loc.course.id + loc.section + h + wd}
            tabIndex="0"
            className="btn btn-sm btn-primary"
            role="button"
            data-toggle="popover"
            data-trigger="focus"
            data-html="true"
            title={loc.course.name}
            data-content={
              loc.teachers.map(t => {
                return `
                  <a
                    target="_blank"
                    href=${pathTeacherDetail.replace('@teacherId@', t.id)}>
                    ${t.grade || ''} ${t.lastName}, ${t.firstName}
                  </a>
                `
              }).join('<br/>') +
              `<br/>Aula: ${loc.classroomsId.join(',')}`
            }>
            {loc.course.id}{loc.section}-{loc.type}
          </a>
        );
    }

    return schedule;
  }

  tbody() {
    const schedule = this.getSchedule();
    return (
      <tbody>
        {
          // TODO: Refactor
          [8,9,10,11,12,13,14,15,16,17,18,19,20,21].map(h => (
            <tr key={h}>
              <th scope="row" className="align-middle">
                {String((h-1)%12+1).padStart(2, '0')}-{String(h%12+1).padStart(2, '0')}
              </th>
              {
                // TODO: Refactor
                [1,2,3,4,5,6].map(wd => (
                  <td key={wd} className="align-middle">
                    {schedule[h][wd]}
                  </td>
                ))
              }
            </tr>
          ))
        }
      </tbody>
    );
  }

  componentDidMount() {
    $('[data-toggle="popover"]').popover();
  }

  render() {
    const {locationList, title} = this.props;

    return (
      <table className="table table-sm table-bordered text-center table-schedule">
        <colgroup>
           <col span="1" style={{width: '7%'}}/>
           <col span="1" style={{width: '15.5%'}}/>
           <col span="1" style={{width: '15.5%'}}/>
           <col span="1" style={{width: '15.5%'}}/>
           <col span="1" style={{width: '15.5%'}}/>
           <col span="1" style={{width: '15.5%'}}/>
           <col span="1" style={{width: '15.5%'}}/>
        </colgroup>
        <thead className="thead-dark">
          <tr>
            <th colSpan="7">
              {title}
            </th>
          </tr>
        </thead>
        <thead>
          <tr>
            <th scope="col"></th>
            <th scope="col">Lun</th>
            <th scope="col">Mar</th>
            <th scope="col">Mié</th>
            <th scope="col">Jue</th>
            <th scope="col">Vie</th>
            <th scope="col">Sáb</th>
          </tr>
        </thead>
        {this.tbody()}
      </table>
    );
  }
}
