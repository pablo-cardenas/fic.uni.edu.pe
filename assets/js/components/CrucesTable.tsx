const $ = require('jquery');
require('bootstrap');
const React = require('react');

declare const pathTeacherDetail: string;

export default class CrucesTable extends (React.Component as { new(any): any }) {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    $('[data-toggle="popover"]').popover();
  }

  render() {
    const {cruces, title} = this.props;

    return (
      <table className="table table-sm table-bordered text-center table-schedule">
        <thead className="thead-dark">
          <tr>
            <th colSpan="7">
              {title}
            </th>
          </tr>
          <tr>
            <th>
              Nº Cruce
            </th>
            <th>
              Curso
            </th>
            <th>
              Sección
            </th>
            <th>
              Tipo
            </th>
            <th>
              Horario
            </th>
          </tr>
        </thead>
        <tbody>
          {cruces.map(([l1, l2], i) => (
            <>
              <tr>
                <th scope="row" rowSpan="2" className="align-middle">
                  {i + 1}
                </th>
                <td>
                  {l1.course.id}
                </td>
                <td>
                  {l1.section}
                </td>
                <td>
                  {l1.type}
                </td>
                <td>
                  {`${l1.beginHour}-${l1.endHour}`}
                </td>
              </tr>
              <tr>
                <td>
                  {l2.course.id}
                </td>
                <td>
                  {l2.section}
                </td>
                <td>
                  {l2.type}
                </td>
                <td>
                  {`${l2.beginHour}-${l2.endHour}`}
                </td>
              </tr>
            </>
          ))}
        </tbody>
      </table>
    );
  }
}
