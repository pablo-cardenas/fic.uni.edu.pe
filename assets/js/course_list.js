import $ from 'jquery';
import 'bootstrap';

const hash = window.location.hash;
hash && $('ul.nav a[href="' + hash + '"]').tab('show');

$('.nav-tabs a').click(function (e) {
  $(this).tab('show');
  const scrollmem = $('body').scrollTop();
  window.location.hash = this.hash;
  $('html,body').scrollTop(scrollmem);
});
