'use strict'

const $ = require('jquery');
require('bootstrap');
const React = require('react');
const ReactDOM =  require('react-dom');

import ScheduleTable from "./components/ScheduleTable";

declare const teacherId: string;
declare const pathTeacherDetail: string;
declare const pathCourseListJson: string;
declare const pathSectionListJson: string;
declare const pathLocationListJson: string;

interface Course {
  id: string;
  name: string;
  cycle: number;
  numCredits: number;
  evaluationSystem: string;
  departmentId: string;
  sectionsId: number[];
}

interface Section {
  course: string;
  section: string;
  locations: Location[];
}

interface Location {
  id: number;
  sectionId: number;
  type: string;
  classroomsId: string;
  weekday; number;
  beginHour: number;
  endHour: number;
}

export default class TeacherTable extends (React.Component as { new(any): any }) {
  constructor(props) {
    super(props)
    this.state = {
      error: null,
      isLoaded: false,
      locationList: [],
    }
  }

  componentDidMount() {
    fetch(pathLocationListJson + '?teacher_id=' + this.props.teacherId)
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            locationList: result.locationList
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

  componentDidUpdate() {
    $('[data-toggle="popover"]').popover();
  }

  render() {
    const {error, isLoaded, locationList} = this.state;

    if (error) {
      return <div>Error al descargar cursos: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Descargando cursos...</div>;
    }

    return (
      <ScheduleTable
        locationList={locationList}
        title={teacherId}/>
    );
  }
}

ReactDOM.render(
  <TeacherTable teacherId={teacherId} />,
  document.getElementById('root')
);
