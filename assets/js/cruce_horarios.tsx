'use strict'
const React = require('react');
const ReactDOM =  require('react-dom');

import ScheduleTable from "./components/ScheduleTable";
import CrucesTable from "./components/CrucesTable";

declare const pathDepartmentDetail: string;
declare const pathCourseDetail: string;
declare const pathCourseList: string;
declare const pathCourseListJson: string;
declare const pathSectionListJson: string;
declare const pathLocationListJson: string;

interface Course {
  id: string;
  name: string;
  cycle: number;
  numCredits: number;
  evaluationSystem: string;
  departmentId: string;
  sectionsId: number[];
}

interface Section {
  course: string;
  section: string;
  locations: Location[];
}

interface Location {
  sectionId: number;
  type: string;
  classroomId: string;
  weekday; number;
  beginHour: number;
  endHour: number;
}

function flatten(arr) {
  return [].concat.apply([], arr)
}

function cartesian_product(pools) {
  return [...pools.entries()].reduce((acc, [course, sections]) => 
    flatten(acc.map(x =>
      [...sections.values()].map(section => new Map([...x, [course, section]]))
  )), [new Map()])
}

class CourseRow extends (React.Component as { new(any): any }) {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      sectionList: [],
    };
  }

  componentDidMount() {
    fetch(pathSectionListJson + '?course_id=' + this.props.course.id)
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            sectionList: result.sectionList
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

  render() {
    function section_td(error, isLoaded, sectionList, selectedSections, onChangeSection) {
      if (error) {
        return <td>Error al descargar secciones: {error.message}</td>;
      } else if (!isLoaded) {
        return <td>Cargando</td>;
      } else {
        return (
          <td>
            {sectionList.map(section => {
              const id = `checkbox_${section.courseId}_${section.section}`;
              return (
                <div
                  key={section.section}
                  className="form-check form-check-inline">
                  <input
                    className="form-check-input"
                    id={id}
                    type="checkbox"
                    value={section.section}
                    defaultChecked={selectedSections.has(section.section)}
                    onChange={() => onChangeSection(section)} />
                  <label
                    className="form-check-label"
                    htmlFor={id}>
                    {section.section}
                  </label>
                </div>
              );
            })}
          </td>
        );
      }
    }

    const {
      course,
      selectedSections,
      onChangeSection,
      onRemoveCourse,
    } = this.props;
    const {error, isLoaded, sectionList} = this.state;

    return (
      <tr className="align-middle">
        <th>
          <a
            target="_blank"
            href={pathCourseDetail.replace('@courseId@', course.id)}>
            {course.id}
          </a>
        </th>
        <td>
          <a
            target="_blank"
            href={pathCourseDetail.replace('@courseId@', course.id)}>
            {course.name}
          </a>
        </td>
        {section_td(error, isLoaded, sectionList, selectedSections, onChangeSection)}
        <td>{course.numCredits}</td>
        <td>
          <a
            target="_blank"
            href={pathCourseList + '#Ciclo_' + course.cycle}>
            {course.cycle}
          </a>
        </td>
        <td>
          <a
            target="_blank"
            href={pathDepartmentDetail.replace('@departmentId@', course.departmentId)}>
            {course.departmentId}
          </a>
        </td>
        <td>
          <button className="btn btn-outline-danger btn-sm" type="button"
                    onClick={() => onRemoveCourse(course.id)}>
            <span className="fa fa-trash"></span>
          </button>
        </td>
      </tr>
    )
  }
}

class App extends (React.Component as { new(any): any }) {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      courseList: [],
      selectedCourses: new Map(),
      inputCourse: '',
    }
  }

  componentDidMount() {
    fetch(pathCourseListJson)
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            courseList: new Map<string, Course>(Object.entries(result)),
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error,
          });
        }
      )
  }

  updateInputCourse(evt) {
    this.setState({
      inputCourse: evt.target.value
    });
  }

  addCourse(courseId: string) {
    if (!this.state.courseList.has(courseId))
      return
    if (this.state.selectedCourses.has(courseId)) {
      this.setState({inputCourse: ''});
      return
    }
    const selectedCourses = new Map(this.state.selectedCourses);
    selectedCourses.set(courseId, new Set);
    this.setState({selectedCourses, inputCourse: ''});
  }

  numDifferentCycles() {
    const {courseList, selectedCourses} = this.state;
    const cycles = [...selectedCourses.keys()].map(courseId =>
      courseList.get(courseId).cycle
    );
    const cycleSet = new Set(cycles)
    return '|{' + [...cycleSet].join(',') + '}|=' + cycleSet.size;
  }

  numCredits() {
    const {courseList, selectedCourses} = this.state;
    return [...selectedCourses.keys()].reduce((acc, courseId) =>
      acc + courseList.get(courseId).numCredits,
      0
    );
  }

  numSchedules() {
    const {selectedCourses} = this.state;
    const numSections = [...selectedCourses.values()].map(sections =>
      sections.size
    );
    const product = numSections.reduce((acc, ns) =>
      acc * ns,
      1
    );
    return numSections.join('×') + " = " + product;
  }

  handleChangeSection(courseId, section) {
    const {selectedCourses} = this.state;
    if (!selectedCourses.has(courseId))
      return;
    if (selectedCourses.get(courseId).has(section))
      selectedCourses.get(courseId).delete(section)
    else
      selectedCourses.get(courseId).add(section)
    this.setState({selectedCourses})
  }

  handleRemoveCourse(courseId) {
    const {selectedCourses} = this.state;
    if (!selectedCourses.has(courseId))
      return;
    selectedCourses.delete(courseId)
    this.setState({selectedCourses})
  }

  renderCourseRow(courseId, selectedSections) {
    const {courseList} = this.state;
    return (
      <CourseRow
        key={courseId}
        course={courseList.get(courseId)}
        selectedSections={selectedSections}
        onChangeSection={section => this.handleChangeSection(courseId, section)}
        onRemoveCourse={section => this.handleRemoveCourse(courseId)} />
    );
  }

  render() {
    const {
      error,
      isLoaded,
      selectedCourses,
      courseList,
    } = this.state

    if (error) {
      return <div>Error al descargar cursos: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Descargando cursos...</div>;
    }

    const rows = [...selectedCourses.entries()].map(e => {
      const [courseId, selectedSections] = e;
      return this.renderCourseRow(courseId, selectedSections);
    });

    const options = [...courseList.values()].map(course => (
      <option key={course.id} value={course.id}>
        {`${course.cycle}) ${course.id}: ${course.name}`}
      </option>
    ));

    const outcomes = cartesian_product(selectedCourses).map((m) => {
      const title = [...m.entries()].map(([course, section]) =>
        course + section.section
      ).join(' - ');

      const key = [...m.entries()].map(([course, section]) =>
        course + section.section
      ).join('_');

      function get_cruces(locations1, locations2) {
        function seCruza(l1, l2) {
          return l1.weekday == l2.weekday &&
            l1.endHour > l2.beginHour && l2.endHour > l1.beginHour;
        }

        // Cartesian product
        const cp = flatten(locations1.map(l1 =>
          locations2.map(l2 =>
            [l1, l2]
        )));

        return cp.filter(([l1, l2]) => seCruza(l1, l2));
      }

      function pairs([x, ...xs]) {
        if (x === undefined)
          return [];
        return xs.map(y => [y, x]).concat(pairs(xs));
      }

      const cruces = flatten(pairs(m).map(([[c1, s1], [c2, s2]]) =>
        get_cruces(s1.locations, s2.locations)
      ));

      const forbiddenTypes = new Set(['PR', 'TA', 'LAB']);

      if (cruces.length > 2 || cruces.some(pair => pair.every(l => forbiddenTypes.has(l.type))))
            return null;
        //return (
        //  <CrucesTable
        //    key={key}
        //    cruces={cruces}
        //    title={title} />
        //);
      else
        return (
          <ScheduleTable
            key={key}
            locationList={flatten([...m.values()].map(s => s.locations))}
            title={title} />
        );
    });

    return (
      <>
        <label htmlFor="input_courses">Código del curso</label>
        <div className="input-group mb-3">
          <input
            type="text"
            list="dl_courses"
            className="form-control"
            id="input_courses"
            placeholder="ciclo) código: Nombre del curso"
            value={this.state.inputCourse}
            onChange={this.updateInputCourse.bind(this)}
            pattern={[...courseList.keys()].join('|')} />
          <div className="input-group-append">
            <button
              className="btn btn-outline-primary"
              type="button"
              id="btn_add"
              onClick={() => this.addCourse(this.state.inputCourse)} >
              <span className="fa fa-plus"></span>
            </button>
          </div>
        </div>
        <datalist id="dl_courses">
          {options}
        </datalist>
        <div className="table-responsive">
          <table className="table table-sm">
            <thead className="thead-dark align-middle">
              <tr>
                <th scope="col">Código</th>
                <th scope="col">Curso</th>
                <th scope="col">Secciones</th>
                <th scope="col">Créd.</th>
                <th scope="col">Ciclo</th>
                <th scope="col">Dept.</th>
                <th scope="col"></th>
              </tr>
            </thead>
            <tbody>
              {rows}
            </tbody>
            <tfoot>
              <tr>
                <th scope="col"></th>
                <th scope="col">{this.state.selectedCourses.size} cursos</th>
                <th scope="col">{this.numSchedules()}</th>
                <th scope="col">{this.numCredits()}</th>
                <th scope="col">{this.numDifferentCycles()}</th>
                <th scope="col"></th>
                <th scope="col"></th>
              </tr>
            </tfoot>
          </table>
        </div>
        {outcomes}
      </>
    );
  }
}

ReactDOM.render(
  <App/>,
  document.getElementById('root')
);
