import $ from 'jquery';
import Popper from 'popper.js'
import 'bootstrap';

import '@fortawesome/fontawesome-free/css/all.min.css';

require('../css/base.scss');

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})


$(function () {
  $('[data-toggle="popover"]').popover()
})

$('.carousel').carousel({
  pause: false,
  interval: 6000
});
