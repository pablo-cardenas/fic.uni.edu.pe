<?php

namespace App\Controller;

use App\Entity\Cms\Announcement;
use App\Entity\Cms\Newsletter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class CmsController extends Controller
{
    /**
     * @Route("/{_locale}/page/entrant_profile", name="cms_page_entrant_profile")
     */
    public function page_entrant_profile()
    {
        return $this->render('cms/page_entrant_profile.html.twig');
    }

    /**
     * @Route("/{_locale}/page/graduate_profile", name="cms_page_graduate_profile")
     */
    public function page_graduate_profile()
    {
        return $this->render('cms/page_graduate_profile.html.twig');
    }

    /**
     * @Route("/{_locale}/page/mission_vision", name="cms_page_mission_vision")
     */
    public function page_mission_vision()
    {
        return $this->render('cms/page_mission_vision.html.twig');
    }

    /**
     * @Route("/{_locale}/page/calendario", name="cms_page_calendario")
     */
    public function page_calendario()
    {
        $announcements = $this->getDoctrine()->getRepository(Announcement::class)->findAll();
        return $this->render('cms/page_calendario.html.twig', [
            'announcements' => $announcements,
        ]);
    }

    /**
     * @Route("/{_locale}/page/dean", name="cms_page_dean")
     */
    public function page_dean()
    {
        return $this->render('cms/page_dean.html.twig');
    }

    /**
     * @Route("/{_locale}/newsletter/{id}", name="cms_newsletter")
     */
    public function newsletter(Newsletter $newsletter)
    {
        $newsletters = $this->getDoctrine()->getRepository(Newsletter::class)->findBy([], ['publishedAt' => 'DESC']);
        return $this->render('cms/newsletter.html.twig', [
            'newsletter' => $newsletter,
            'newsletters' => $newsletters,
        ]);
    }

    /**
     * @Route("/{_locale}/announcement/{id}", name="cms_announcement")
     */
    public function announcement(Announcement $announcement)
    {
        $announcements = $this->getDoctrine()->getRepository(Announcement::class)->findAll();
        return $this->render('cms/announcement.html.twig', [
            'announcements' => $announcements,
            'announcement' => $announcement,
        ]);
    }
}
