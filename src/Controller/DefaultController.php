<?php
namespace App\Controller;

use App\Entity\Cms\Newsletter;
use App\Entity\Cms\Announcement;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="root")
     */
    public function root(Request $request)
    {
        return $this->redirectToRoute('index', ["_locale" => "es"]);
    }

    /**
     * @Route(
     *     "/{_locale}/",
     *     requirements={"_locale":"%locales%"},
     *     name="index"
     *  )
     */
    public function index(Request $request)
    {
        $announcements = $this->getDoctrine()->getRepository(Announcement::class)->findAll();
        $newsletters = $this->getDoctrine()->getRepository(Newsletter::class)->findAll();
        return $this->render('index.html.twig', [
            'announcements' => $announcements,
            'newsletters' => $newsletters,
        ]);
    }
}
