<?php
namespace App\Controller;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityRepository;
use App\Entity\Teacher\Teacher;
use App\Entity\Department;
use App\Entity\Course\Location;
use App\Entity\Course\Course;
use App\Entity\Course\Section;

class CruceHorariosController extends Controller
{
    /**
     * @Route("/{_locale}/cruce-horarios/", name="cruce_horarios")
     */
    public function cruceHorarios()
    {
        return $this->render('courses/cruce_horarios.html.twig');
    }

    /**
     * @Route("/cruce-horarios/course_list.json", name="course_list_json")
     */
    public function CourseList()
    {
        $courses  = $this->getDoctrine()->getRepository(Course::class)->findAll();

        $coursesList = [];
        foreach ($courses as $c) {
            $coursesList[$c->getId()] = self::courseJson($c);
        }
        return $this->json($coursesList);
    }

    /**
     * @Route("/cruce-horarios/section_list.json", name="section_list_json")
'    */
    public function sectionList(Request $request)
    {
        $courseId = $request->query->get('course_id');
        $course   = $this->getDoctrine()->getRepository(Course::class)->find($courseId);

        $ret['course'] = self::courseJson($course);

        foreach ($course->getSections() as $s) {
            $ret['sectionList'][] = [
                'courseId' => $s->getCourse()->getId(),
                'section' => $s->getSection(),
                'locations' => $s->getLocations()->map(function($l) {
                    return self::locationJson($l);
                })->toArray(),
            ];
        }
        return $this->json($ret);
    }

    /**
     * @Route("/cruce-horarios/location_list.json", name="location_list_json")
'    */
    public function locationList(Request $request)
    {
        $teacherId = $request->query->get('teacher_id');
        $teacher   = $this->getDoctrine()->getRepository(Teacher::class)->find($teacherId);

        $locationsList = [];
        foreach ($teacher->getLocations() as $l) {
            $locationsList['locationList'][] = self::locationJson($l);
        }
        return $this->json($locationsList);
    }

    static function courseJson($course) {
        return [
            'id' => $course->getId(),
            'name' => $course->getNameEs(),
            'cycle' => $course->getCycle(),
            'numCredits' => $course->getNumCredits(),
            'evaluationSystem' => $course->getEvaluationSystem()->getId(),
            'departmentId' => $course->getDepartment()->getId(),
            'sectionsId' => $course->getSections()->map(function($s) {
                return $s->getId();
            })->toArray(),
        ];
    }

    static function locationJson($location) {
        return [
            'course' => [
                'id' => $location->getSection()->getCourse()->getId(),
                'name' => $location->getSection()->getCourse()->getNameEs(),
            ],
            'section' => $location->getSection()->getSection(),
            'type' => $location->getType(),
            'classroomsId' => $location->getClassrooms()->map(function($cr) {
                return $cr->getId();
            })->toArray(),
            'teachers' => $location->getTeachers()->map(function($t) {
                return [
                    'id' => $t->getId(),
                    'grade' => $t->getGrade(),
                    'lastName' => $t->getLastName(),
                    'firstName' => $t->getFirstName(),
                ];
            })->toArray(),
            'weekday' => $location->getWeekday(),
            'beginHour' => $location->getBeginHour(),
            'endHour' => $location->getEndHour(),
        ];
    }
}
