<?php
namespace App\Controller;

use App\Entity\Cms\Announcement;
use App\Entity\Cms\Newsletter;
use App\Entity\Cms\NewsletterType;
use App\Entity\Course\Course;
use App\Entity\Course\Location;
use App\Entity\Department;
use App\Entity\Teacher\Category;
use App\Entity\Teacher\Teacher;
use Doctrine\ORM\EntityRepository;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class AdminController extends Controller
{
    /**
     * @Route("/admin", name="root_admin")
     */
    public function root(Request $request)
    {
        return $this->redirectToRoute('admin', ["_locale" => "es"]);
    }

    /**
     * @Route("/{_locale}/admin", name="admin")
     */
    public function admin()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();
        $role = $user->getRoles()[0];

        if ($role == "ROLE_DEPARTMENT") {
            return $this->redirectToRoute('department_dashboard', [
                'id' => $user->getUsername()
            ]);
        }
        else if ($role == "ROLE_STAFF") {
            return $this->redirectToRoute('staff_dashboard');
        }
        else if ($role == "ROLE_SCHOOL") {
            return $this->redirectToRoute('school_dashboard');
        }
        else if ($role == "ROLE_PRESS") {
            return $this->redirectToRoute('press_dashboard');
        }

        return new Response('<html><body>Admin page!</body></html>');
    }

    /**
     * @Route("{_locale}/admin/department/{id}", name="department_dashboard")
     */
    public function departmentDashboard(Department $department)
    {
        $this->denyAccessUnlessGranted('ROLE_DEPARTMENT');
        $username = $this->getUser()->getUsername();
        $department = $this->getDoctrine()->getRepository(Department::class)->find($username);

        if ($department->getId() != $username) {
            throw $this->createAccessDeniedException("You aren't in this department");
        }

        return $this->render('admin/department_dashboard.html.twig', [
            'department' => $department
        ]);
    }

    /**
     * @Route("{_locale}/admin/staff/", name="staff_dashboard")
     */
    public function staffDashboard()
    {
        $this->denyAccessUnlessGranted('ROLE_STAFF');

        $departments = $this->getDoctrine()->getRepository(Department::class)->findAll();

        return $this->render('admin/staff_dashboard.html.twig', [
            'departments' => $departments,
        ]);
    }

    /**
     * @Route("{_locale}/admin/school/", name="school_dashboard")
     */
    public function schoolDashboard()
    {
        $this->denyAccessUnlessGranted('ROLE_SCHOOL');

        return $this->render('admin/school_dashboard.html.twig');
    }

    /**
     * @Route("{_locale}/admin/press/", name="press_dashboard")
     */
    public function pressDashboard()
    {
        $this->denyAccessUnlessGranted('ROLE_PRESS');

        $newsletters = $this->getDoctrine()->getRepository(Newsletter::class)->findBy([], ['publishedAt' => 'DESC']);

        return $this->render('admin/press_dashboard.html.twig', [
            'newsletters'=> $newsletters,
        ]);
    }

    /**
     * @Route("/{_locale}/admin/teacher/photo_update/{id}", name="teacher_photo_update")
     */
    public function teacherPhotoUpdate(Teacher $teacher, Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_DEPARTMENT');
        $user = $this->getUser();

        if (!$teacher->getDepartments()
            ->map(function($d) { return $d->getId(); })
            ->contains($user->getUsername())) {
            throw $this->createAccessDeniedException("You aren't allowed to edit this teacher.");
        }

        $form = $this->createFormBuilder([])
            ->add('image', FileType::class, [
                'label' => 'Foto (imagen JPG)'
            ])
            ->add('save', SubmitType::class, [
                'label' => 'OK'
            ])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
            $file = $form->getData()['image'];

            $fileName = $teacher->getId().'.jpg';
            $directory = $this->getParameter('photos_directory').'/teacher';

            // Delete if exist and move
            unlink($directory . '/' . $fileName);
            $file->move($directory, $fileName);

            return $this->redirectToRoute('teacher_list');
        }

        return $this->render('admin/teacher_photo_update.html.twig', [
            'teacher' => $teacher,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{_locale}/admin/teacher/summary_update/{id}", name="teacher_summary_update")
     */
    public function teacherSummaryUpdate(Teacher $teacher, Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_DEPARTMENT');
        $user = $this->getUser();

        if (!$teacher->getDepartments()
            ->map(function($d) { return $d->getId(); })
            ->contains($user->getUsername())) {
            throw $this->createAccessDeniedException("You aren't allowed to edit this teacher.");
        }

	$form = $this->createFormBuilder($teacher)
	    ->add('summaryEs', TextareaType::class)
	    ->add('summaryEn', TextareaType::class)
	    ->add('save', SubmitType::class, ['label' => 'Update Teacher'])
	    ->getForm();

	$form->handleRequest($request);

	if ($form->isSubmitted() && $form->isValid()) {
	    $teacher = $form->getData();

	    $entityManager = $this->getDoctrine()->getManager();
	    $entityManager->persist($teacher);
	    $entityManager->flush();

	    return $this->redirectToRoute('department_dashboard');
	}

	return $this->render('admin/teacher_update.html.twig', [
            'teacher' => $teacher,
	    'form' => $form->createView(),
	]);
    }

    /**
     * @Route("/{_locale}/admin/teacher/update/{id}", name="teacher_update")
     */
    public function teacherUpdate(Teacher $teacher, Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_STAFF');

	$form = $this->createFormBuilder($teacher)
	    ->add('firstName', TextType::class)
	    ->add('lastName', TextType::class)
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'choice_label' => 'nameEs',
            ])
	    ->add('email', TextType::class)
	    ->add('grade', TextType::class)
	    ->add('birthday', DateType::class, ['years' => range(1930, date('Y'))])
	    ->add('phone', TextType::class)
	    ->add('save', SubmitType::class, ['label' => 'Update Teacher'])
	    ->getForm();

	$form->handleRequest($request);

	if ($form->isSubmitted() && $form->isValid()) {
	    $teacher = $form->getData();

	    $entityManager = $this->getDoctrine()->getManager();
	    $entityManager->persist($teacher);
	    $entityManager->flush();

	    return $this->redirectToRoute('staff_dashboard');
	}

	return $this->render('admin/teacher_update.html.twig', [
            'teacher' => $teacher,
	    'form' => $form->createView(),
	]);
    }

    /**
     * @Route("/{_locale}/admin/department/update/{id}", name="department_update")
     */
    public function departmentUpdate(Department $department, Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_DEPARTMENT');

        if ($department->getId() != $this->getUser()->getUsername()) {
            throw $this->createAccessDeniedException("You aren't allowed to edit this department.");
        }

	$form = $this->createFormBuilder($department)
	    ->add('summaryEs', TextAreaType::class)
	    ->add('summaryEn', TextAreaType::class)
	    ->add('OrganizationEs', TextAreaType::class)
	    ->add('OrganizationEn', TextAreaType::class)
	    ->add('save', SubmitType::class, ['label' => 'Update department'])
	    ->getForm();

	$form->handleRequest($request);

	if ($form->isSubmitted() && $form->isValid()) {
	    $department = $form->getData();

	    $entityManager = $this->getDoctrine()->getManager();
	    $entityManager->persist($department);
	    $entityManager->flush();

	    return $this->redirectToRoute('department_dashboard', ['id' => $department->getId()]);
	}

	return $this->render('admin/department_update.html.twig', [
	    'form' => $form->createView(),
	]);
    }

    /**
     * @Route("/{_locale}/admin/announcement/create/", name="announcement_create")
     */
    public function announcementCreate(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_SCHOOL');

        $form = $this->createFormBuilder([])
            ->add('id', TextType::class, [
                'label' => 'No-Year'
            ])
            ->add('title', TextType::class, [
                'label' => 'Title'
            ])
            ->add('image', FileType::class, [
                'label' => 'Foto (imagen JPG)'
            ])
            ->add('save', SubmitType::class, [
                'label' => 'OK'
            ])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
            $file = $form->getData()['image'];
            $id = $form->getData()['id'];
            $title = $form->getData()['title'];

            $fileName = $id.'.jpg';
            $directory = $this->getParameter('cms_directory').'/announcement/';

            $file->move($directory, $fileName);

            $em = $this->getDoctrine()->getManager();
            $announcement = new Announcement();
            $announcement->setId($id);
            $announcement->setTitle($title);
            $em->persist($announcement);
            $em->flush();

            return $this->redirectToRoute('school_dashboard');
        }

        return $this->render('admin/announcement_create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{_locale}/admin/newsletter/create/", name="newsletter_create")
     */
    public function newsletterCreate(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_PRESS');

        $newsletter =  new Newsletter();

        $form = $this->createFormBuilder($newsletter)
            ->add('type', EntityType::class, [
                'label' => 'Tipo',
                'class' => NewsletterType::class,
                'choice_label' => 'name',
            ])
            ->add('number', IntegerType::class, [
                'label' => 'Número',
            ])
            ->add('issuuCode', IntegerType::class, [
                'label' => 'Código Issuu'
            ])
            ->add('issuuImageCode', TextType::class, [
                'label' => 'Código de la imagen'
            ])
            ->add('publishedAt', DateType::class, [
                'years' => range(2013, date('Y')),
                'data' => new \DateTime(),
                'label' => 'Fecha de Publicación'
            ])
            ->add('save', SubmitType::class, [
                'label' => 'OK'
            ])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $newsletter = $form->getData();
	    $em = $this->getDoctrine()->getManager();
            $em->persist($newsletter);
	    $em->flush();

	    return $this->redirectToRoute('press_dashboard');
        }

        return $this->render('admin/announcement_create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{_locale}/admin/newsletter/update/{id}", name="newsletter_update")
     */
    public function newsletterUpdate(Newsletter $newsletter, Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_PRESS');

        $form = $this->createFormBuilder($newsletter)
            ->add('type', EntityType::class, [
                'label' => 'Tipo',
                'class' => NewsletterType::class,
                'choice_label' => 'name',
            ])
            ->add('number', IntegerType::class, [
                'label' => 'Número',
            ])
            ->add('issuuCode', IntegerType::class, [
                'label' => 'Código Issuu'
            ])
            ->add('issuuImageCode', TextType::class, [
                'label' => 'Código de la imagen'
            ])
            ->add('publishedAt', DateType::class, [
                'years' => range(2013, date('Y')),
                'label' => 'Fecha de Publicación'
            ])
            ->add('save', SubmitType::class, [
                'label' => 'OK'
            ])
            ->getForm();

	$form->handleRequest($request);

	if ($form->isSubmitted() && $form->isValid()) {
	    $newsletter = $form->getData();

	    $em = $this->getDoctrine()->getManager();
	    $em->persist($newsletter);
	    $em->flush();

	    return $this->redirectToRoute('press_dashboard');
	}

	return $this->render('admin/newsletter_form.html.twig', [
	    'form' => $form->createView(),
	]);
    }
    /**
     * @Route("/{_locale}/admin/newsletter/delete/{id}", name="newsletter_delete")
     */
    public function newsletterDelete(Newsletter $newsletter, Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_PRESS');

        $form = $this->createFormBuilder()
            ->add('save', SubmitType::class, [
                'label' => 'Estoy Seguro de borrar'
            ])
            ->getForm();

	$form->handleRequest($request);

	if ($form->isSubmitted() && $form->isValid()) {
	    $em = $this->getDoctrine()->getManager();
            $em->remove($newsletter);
            $em->flush();

	    return $this->redirectToRoute('press_dashboard');
	}

	return $this->render('admin/newsletter_confirm_delete.html.twig', [
	    'form' => $form->createView(),
	]);
    }
}
