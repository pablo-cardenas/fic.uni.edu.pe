<?php
namespace App\Controller;

use App\Entity\Course\Course;
use App\Entity\Course\Location;
use App\Entity\Department;
use App\Entity\Teacher\Teacher;
use App\Entity\Cms\Announcement;
use App\Entity\Cms\Comunidad;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CourseController extends Controller
{
    const TEMPLATE_DIRECTORY = 'courses/';

    /**
     * @Route("/{_locale}/teachers/", name="teacher_list")
     */
    public function teacherList()
    {
        $departments = $this->getDoctrine()->getRepository(Department::class)->findAll();

        return $this->render(self::TEMPLATE_DIRECTORY . 'teacher_list.html.twig', [
            'departments' => $departments,
        ]);
    }

    /**
     * @Route("/{_locale}/teacher/{id}", name="teacher_detail")
     */
    public function teacherDetail(Teacher $teacher)
    {
        foreach($teacher->getDepartments() as $department) {
            $id = $department->getId();
            $name = $department->getName();
            $departments[] = "<a href=\"/departamento/$id\">Departamento Académico de $name</a>";
        }

        $sections = [];
        foreach($teacher->getLocations() as $location) {
            $id = $department->getId();
            $name = $department->getName();
            //$departments[] = "<a href=\"/departamento/$id\">Departamento Académico de $name</a>";
            $sections[] = $location->getSection();
        }

        $sections = array_unique($sections, SORT_REGULAR);

        return $this->render(self::TEMPLATE_DIRECTORY . 'teacher_detail.html.twig', [
            'teacher' => $teacher,
            'departments' => $departments,
            'sections' => $sections,
        ]);
    }

    /**
     * @Route("/{_locale}/courses/", name="course_list")
     */
    public function courseList()
    {
        $courses = $this->getDoctrine()->getRepository(Course::class)->findAll();

        $coursesByCycle = [];
        foreach ($courses as $course) {
            $coursesByCycle[$course->getCycle()][] = $course;
        }

        return $this->render(self::TEMPLATE_DIRECTORY . 'course_list.html.twig', [
            'coursesByCycle' => $coursesByCycle,
        ]);
    }

    /**
     * @Route("/{_locale}/course/{id}", name="course_detail")
     */
    public function courseDetail(Course $course)
    {
        $courses = $this->getDoctrine()->getRepository(Course::class)->findAll();

        return $this->render(self::TEMPLATE_DIRECTORY . 'course_detail.html.twig', [
            'course' => $course,
        ]);
    }

    /**
     * @Route("/{_locale}/departments/", name="department_list")
     */
    public function departmentList()
    {
        $departments = $this->getDoctrine()->getRepository(Department::class)->findAll();

        return $this->render(self::TEMPLATE_DIRECTORY . 'department_list.html.twig', [
            'departments' => $departments,
        ]);
    }

    /**
     * @Route("/{_locale}/department/{id}", name="department_detail")
     */
    public function departmentDetail(Department $department)
    {
        return $this->render(self::TEMPLATE_DIRECTORY . 'department_detail.html.twig', [
            'department' => $department,
        ]);
    }
}
