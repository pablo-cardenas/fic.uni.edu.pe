<?php

namespace App\Repository\Teacher;

use App\Entity\Teacher\Dedication;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Dedication|null find($id, $lockMode = null, $lockVersion = null)
 * @method Dedication|null findOneBy(array $criteria, array $orderBy = null)
 * @method Dedication[]    findAll()
 * @method Dedication[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DedicationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Dedication::class);
    }

//    /**
//     * @return Dedication[] Returns an array of Dedication objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Dedication
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
