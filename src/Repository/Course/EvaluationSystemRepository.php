<?php

namespace App\Repository\Course;

use App\Entity\Course\EvaluationSystem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EvaluationSystem|null find($id, $lockMode = null, $lockVersion = null)
 * @method EvaluationSystem|null findOneBy(array $criteria, array $orderBy = null)
 * @method EvaluationSystem[]    findAll()
 * @method EvaluationSystem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EvaluationSystemRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EvaluationSystem::class);
    }

//    /**
//     * @return EvaluationSystem[] Returns an array of EvaluationSystem objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EvaluationSystem
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
