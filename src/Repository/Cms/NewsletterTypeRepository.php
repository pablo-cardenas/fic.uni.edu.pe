<?php

namespace App\Repository\Cms;

use App\Entity\Cms\NewsletterType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method NewsletterType|null find($id, $lockMode = null, $lockVersion = null)
 * @method NewsletterType|null findOneBy(array $criteria, array $orderBy = null)
 * @method NewsletterType[]    findAll()
 * @method NewsletterType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NewsletterTypeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, NewsletterType::class);
    }

//    /**
//     * @return NewsletterType[] Returns an array of NewsletterType objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?NewsletterType
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
