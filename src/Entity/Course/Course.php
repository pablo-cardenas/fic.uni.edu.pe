<?php

namespace App\Entity\Course;

use App\Entity\Department;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Course\CourseRepository")
 */
class Course
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=15)
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $nameEs;

    /**
     * @ORM\Column(type="string")
     */
    private $nameEn;

    /**
     * @ORM\Column(type="smallint")
     */
    private $numCredits;

    /**
     * @ORM\Column(type="smallint")
     */
    private $cycle;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Course\Section", mappedBy="course", orphanRemoval=true)
     */
    private $sections;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Course\EvaluationSystem", inversedBy="courses")
     * @ORM\JoinColumn(nullable=false)
     */
    private $evaluationSystem;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Department", inversedBy="courses")
     * @ORM\JoinColumn(nullable=false)
     */
    private $department;

    public function __construct()
    {
        $this->sections = new ArrayCollection();
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getNameEs(): string
    {
        return $this->nameEs;
    }

    public function setNameEs(string $nameEs): self
    {
        $this->nameEs = $nameEs;

        return $this;
    }

    public function getNameEn(): string
    {
        return $this->nameEn;
    }

    public function setNameEn(string $nameEn): self
    {
        $this->nameEn = $nameEn;

        return $this;
    }

    public function getNumCredits(): int
    {
        return $this->numCredits;
    }

    public function setNumCredits(int $numCredits): self
    {
        $this->numCredits = $numCredits;

        return $this;
    }

    public function getCycle(): int
    {
        return $this->cycle;
    }

    public function setCycle(int $cycle): self
    {
        $this->cycle = $cycle;

        return $this;
    }

    /**
     * @return Collection|Section[]
     */
    public function getSections(): Collection
    {
        return $this->sections;
    }

    public function addSection(Section $section): self
    {
        if (!$this->sections->contains($section)) {
            $this->sections[] = $section;
            $section->setCourse($this);
        }

        return $this;
    }

    public function removeSection(Section $section): self
    {
        if ($this->sections->contains($section)) {
            $this->sections->removeElement($section);
            // set the owning side to null (unless already changed)
            if ($section->getCourse() === $this) {
                $section->setCourse(null);
            }
        }

        return $this;
    }

    public function getEvaluationSystem(): EvaluationSystem
    {
        return $this->evaluationSystem;
    }

    public function setEvaluationSystem(EvaluationSystem $evaluationSystem): self
    {
        $this->evaluationSystem = $evaluationSystem;

        return $this;
    }

    public function getDepartment(): Department
    {
        return $this->department;
    }

    public function setDepartment(Department $department): self
    {
        $this->department = $department;

        return $this;
    }
}
