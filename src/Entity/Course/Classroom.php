<?php

namespace App\Entity\Course;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Course\ClassroomRepository")
 * @ORM\Table(name="course_classroom")
 */
class Classroom
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=15)
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Course\Location", mappedBy="classrooms")
     */
    private $locations;

    public function __construct()
    {
        $this->locations = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Collection|Location[]
     */
    public function getLocations(): Collection
    {
        return $this->locations;
    }

    public function addLocation(Location $location): self
    {
        if (!$this->locations->contains($location)) {
            $this->locations[] = $location;
            $location->addClassroom($this);
        }

        return $this;
    }

    public function removeLocation(Location $location): self
    {
        if ($this->locations->contains($location)) {
            $this->locations->removeElement($location);
            $location->removeClassroom($this);
        }

        return $this;
    }
}
