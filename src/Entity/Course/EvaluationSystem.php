<?php

namespace App\Entity\Course;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Course\EvaluationSystemRepository")
 * @ORM\Table(name="course_evaluation_system")
 */
class EvaluationSystem
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=1)
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Course\Course", mappedBy="evaluationSystem")
     */
    private $courses;

    public function __construct()
    {
        $this->courses = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Collection|Course[]
     */
    public function getCourses(): Collection
    {
        return $this->courses;
    }

    public function addCourse(Course $course): self
    {
        if (!$this->courses->contains($course)) {
            $this->courses[] = $course;
            $course->setEvaluationSystem($this);
        }

        return $this;
    }

    public function removeCourse(Course $course): self
    {
        if ($this->courses->contains($course)) {
            $this->courses->removeElement($course);
            // set the owning side to null (unless already changed)
            if ($course->getEvaluationSystem() === $this) {
                $course->setEvaluationSystem(null);
            }
        }

        return $this;
    }
}
