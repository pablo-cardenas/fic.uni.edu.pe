<?php

namespace App\Entity\Teacher;

use App\Entity\Course\Location;
use App\Entity\Department;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Teacher\TeacherRepository")
 */
class Teacher
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=15)
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $firstName;

    /**
     * @ORM\Column(type="string")
     */
    private $lastName;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $summaryEs;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $summaryEn;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $birthday;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $grade;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $teacherCondition;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Department", inversedBy="teachers")
     */
    private $departments;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $phone;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Course\Location", mappedBy="teachers")
     */
    private $locations;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Teacher\Category", inversedBy="teachers")
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Teacher\Dedication", inversedBy="teachers")
     */
    private $dedication;

    public function __construct()
    {
        $this->departments = new ArrayCollection();
        $this->locations = new ArrayCollection();
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getSummaryEs()
    {
        return $this->summaryEs;
    }

    public function setSummaryEs(string $summaryEs): self
    {
        $this->summaryEs = $summaryEs;

        return $this;
    }

    public function getSummaryEn()
    {
        return $this->summaryEn;
    }

    public function setSummaryEn(string $summaryEn): self
    {
        $this->summaryEn = $summaryEn;

        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getBirthday(): \DateTimeInterface
    {
        return $this->birthday;
    }

    public function setBirthday(\DateTimeInterface $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getGrade()
    {
        return $this->grade;
    }

    public function setGrade(string $grade): self
    {
        $this->grade = $grade;

        return $this;
    }

    public function getTeacherCondition()
    {
        return $this->teacherCondition;
    }

    public function setTeacherCondition(string $teacherCondition): self
    {
        $this->teacherCondition = $teacherCondition;

        return $this;
    }

    /**
     * @return Collection|Department[]
     */
    public function getDepartments(): Collection
    {
        return $this->departments;
    }

    public function addDepartment(Department $department): self
    {
        if (!$this->departments->contains($department)) {
            $this->departments[] = $department;
        }

        return $this;
    }

    public function removeDepartment(Department $department): self
    {
        if ($this->departments->contains($department)) {
            $this->departments->removeElement($department);
        }

        return $this;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return Collection|Location[]
     */
    public function getLocations(): Collection
    {
        return $this->locations;
    }

    public function addLocation(Location $location): self
    {
        if (!$this->locations->contains($location)) {
            $this->locations[] = $location;
            $location->addTeacher($this);
        }

        return $this;
    }

    public function removeLocation(Location $location): self
    {
        if ($this->locations->contains($location)) {
            $this->locations->removeElement($location);
            $location->removeTeacher($this);
        }

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getDedication(): ?Dedication
    {
        return $this->dedication;
    }

    public function setDedication(?Dedication $dedication): self
    {
        $this->dedication = $dedication;

        return $this;
    }
}
