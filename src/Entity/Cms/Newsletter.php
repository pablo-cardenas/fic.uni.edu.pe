<?php

namespace App\Entity\Cms;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Cms\NewsletterRepository")
 * @ORM\Table(name="cms_newsletter")
 */
class Newsletter
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $publishedAt;

    /**
     * @ORM\Column(type="integer")
     */
    private $issuuCode;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $issuuImageCode;

    /**
     * @ORM\Column(type="integer")
     */
    private $number;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Cms\NewsletterType", inversedBy="newsletters")
     * @ORM\JoinColumn(nullable=false)
     */
    private $type;

    public function getId()
    {
        return $this->id;
    }

    public function getPublishedAt(): ?\DateTimeInterface
    {
        return $this->publishedAt;
    }

    public function setPublishedAt(\DateTimeInterface $publishedAt): self
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    public function getIssuuCode(): ?int
    {
        return $this->issuuCode;
    }

    public function setIssuuCode(int $issuuCode): self
    {
        $this->issuuCode = $issuuCode;

        return $this;
    }

    public function getIssuuImageCode(): ?string
    {
        return $this->issuuImageCode;
    }

    public function setIssuuImageCode(string $issuuImageCode): self
    {
        $this->issuuImageCode = $issuuImageCode;

        return $this;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function setNumber(int $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getType(): ?NewsletterType
    {
        return $this->type;
    }

    public function setType(?NewsletterType $type): self
    {
        $this->type = $type;

        return $this;
    }
}
