<?php

namespace App\Entity;

use App\Entity\Course\Course;
use App\Entity\Teacher\Teacher;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DepartmentRepository")
 */
class Department
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=15)
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Teacher\Teacher", mappedBy="departments")
     */
    private $teachers;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Course\Course", mappedBy="department")
     */
    private $courses;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $summaryEs;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $summaryEn;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $organizationEs;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $organizationEn;

    public function __construct()
    {
        $this->teachers = new ArrayCollection();
        $this->courses = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Teacher[]
     */
    public function getTeachers(): Collection
    {
        return $this->teachers;
    }

    public function addTeacher(Teacher $teacher): self
    {
        if (!$this->teachers->contains($teacher)) {
            $this->teachers[] = $teacher;
            $teacher->addDepartment($this);
        }

        return $this;
    }

    public function removeTeacher(Teacher $teacher): self
    {
        if ($this->teachers->contains($teacher)) {
            $this->teachers->removeElement($teacher);
            $teacher->removeDepartment($this);
        }

        return $this;
    }

    /**
     * @return Collection|Course[]
     */
    public function getCourses(): Collection
    {
        return $this->courses;
    }

    public function addCourse(Course $course): self
    {
        if (!$this->courses->contains($course)) {
            $this->courses[] = $course;
            $course->setDepartment($this);
        }

        return $this;
    }

    public function removeCourse(Course $course): self
    {
        if ($this->courses->contains($course)) {
            $this->courses->removeElement($course);
            // set the owning side to null (unless already changed)
            if ($course->getDepartment() === $this) {
                $course->setDepartment(null);
            }
        }

        return $this;
    }

    public function getSummaryEs()
    {
        return $this->summaryEs;
    }

    public function setSummaryEs(string $summaryEs): self
    {
        $this->summaryEs = $summaryEs;

        return $this;
    }

    public function getSummaryEn()
    {
        return $this->summaryEn;
    }

    public function setSummaryEn(string $summaryEn): self
    {
        $this->summaryEn = $summaryEn;

        return $this;
    }

    public function getOrganizationEs()
    {
        return $this->organizationEs;
    }

    public function setOrganizationEs(string $organizationEs): self
    {
        $this->organizationEs = $organizationEs;

        return $this;
    }

    public function getOrganizationEn()
    {
        return $this->organizationEn;
    }

    public function setOrganizationEn(string $organizationEn): self
    {
        $this->organizationEn = $organizationEn;

        return $this;
    }
}
